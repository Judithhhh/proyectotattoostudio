import javax.xml.crypto.Data;
import java.util.Date;
import java.util.List;

public class Estudi {

    /**
     * @author Marti Serrallach
     */

    String nom;
    String ciutat;
    String cif;
    Date data;
    int id;

    public Estudi(String nom, String ciutat, String cif, Date date) {
        this.nom = nom;
        this.ciutat = ciutat;
        this.cif = cif;
        this.data = date;
    }

    public Estudi() {
    }
}
