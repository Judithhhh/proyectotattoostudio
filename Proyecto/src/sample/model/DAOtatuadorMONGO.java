import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import org.bson.Document;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Filters.gt;

    /**
     * @author Marti Serrallach & Judith Archilla
     * 
     * Mètodes que gestionen els tatuadors en Mongo.
     */

public class DAOtatuadorMONGO implements DAOtatuador {

    ConexioMongo conexioMongo = new ConexioMongo();

    /**
     * Mètode per recuperar per Id.
     *
     * @param id identificador.
     * @return
     */
    @Override
    public Tatuador recuperaPerId(Integer id){
        Tatuador tatuador = new Tatuador();
        MongoDatabase mongoDatabase = conexioMongo.connection();
        MongoCollection collection = mongoDatabase.getCollection("studio");
        try(MongoCursor<Document> cursor = collection.find(gt("id",id)).iterator()){
            while (cursor.hasNext()){
                Document document = cursor.next();
                tatuador.id = document.getInteger("id");
                tatuador.nom = document.getString("nom");
                tatuador.nif = document.getInteger("nif");
                tatuador.estil = document.getString("estil");
                tatuador.estudi = document.getString("estudi");
                tatuador.actiu = document.getBoolean("actiu");
                tatuador.instagram = document.getString("instagram");
            }
        }finally {
            conexioMongo.close();
        }
        return tatuador;
    }

    /**
     * Mètode per recuperar dades.
     *
     * @return llista de tots els tatuadors
     */
    @Override
    public List<Tatuador> recuperarTots(){
        List<Tatuador> tatuadors = new ArrayList<>();
        MongoDatabase mongoDatabase = conexioMongo.connection();
        MongoCollection collection = mongoDatabase.getCollection("studio");
        try(MongoCursor<Document> cursor = collection.find().iterator()){
            while (cursor.hasNext()){
                Tatuador tatuador = new Tatuador();
                Document document = cursor.next();
                tatuador.id = document.getInteger("id");
                tatuador.nom = document.getString("nom");
                tatuador.nif = document.getInteger("nif");
                tatuador.estil = document.getString("estil");
                tatuador.estudi = document.getString("estudi");
                tatuador.actiu = document.getBoolean("actiu");
                tatuador.instagram = document.getString("instagram");
                tatuadors.add(tatuador);
            }
        }finally {
            conexioMongo.close();
        }
        return tatuadors;
    }

    /**
     * Mètode per agregar un tatuador.
     *
     * @param inserir
     */
    @Override
    public void insereix(Tatuador inserir){
        MongoDatabase mongoDatabase = conexioMongo.connection();
        MongoCollection<Document> collection = mongoDatabase.getCollection("tattoo_artist");
        Document doc = new Document("id",inserir.id).append("nom",inserir.nom).append("nif",inserir.nif).append("estil",inserir.estil).append("estudi",inserir.estudi).append("actiu",inserir.actiu).append("instagram",inserir.instagram);
        collection.insertOne(doc);
        conexioMongo.close();
    }

    /**
     * Mètode per eliminar un tatuador.
     *
     * @param id identificador.
     */
    @Override
    public void eliminar(Integer id){
        MongoDatabase mongoDatabase = conexioMongo.connection();
        MongoCollection<Document> collection = mongoDatabase.getCollection("tattoo_artist");
        DeleteResult deleteResult = collection.deleteMany(gt("id",id));
        conexioMongo.close();
    }

    /**
     * Mètode per actualitzar.
     *
     * @param estudi
     */
    @Override
    public void update(Tatuador estudi){

    }
}
