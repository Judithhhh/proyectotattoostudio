import com.mongodb.client.MongoDatabase;

public interface DAOconexioMONGO extends Conexio{
    /**
     * @author Judith Archilla & Marti Serrallach
     *
     * Interface dels mètodes per a connectar Mongo.
     */

    public MongoDatabase connection();
}
