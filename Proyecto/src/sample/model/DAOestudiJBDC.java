import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Scanner;

public class DAOestudiJBDC implements DAOestudi{

    /**
     * @author Marti Serrallach
     *
     * Mètodes que gestionen els tatuadors en JBDC.
     */

    ConexioJBDC conexioJBDC = new ConexioJBDC();
    Excepcions excepcions = new Excepcions();

    /**
     * Mètode per a buscar per nom
     *
     * @param nom
     * @return
     * @throws SQLException
     */
    @Override
    public List<Estudi> cercaPerNom(String nom) throws SQLException {
        conexioJBDC.connection();
        ArrayList<Estudi> estudis = new ArrayList<>();

        String sentencia = "select * from ESTUDIS where NOM = "+nom+";";
        Statement statementCl = this.conexioJBDC.connection.createStatement();
        ResultSet rsEst = statementCl.executeQuery(sentencia);

        while (rsEst.next()){
            Estudi estudi = new Estudi();
            estudi.nom = rsEst.getString("NOM");
            estudi.ciutat = rsEst.getString("CIUTAT");
            estudi.cif = rsEst.getString("CIF");
            estudi.data = rsEst.getDate("DATA_ALTA");
            estudis.add(estudi);
        }
        conexioJBDC.close();
        return estudis;
    }

    /**
     * Mètode per a inserir dades.
     *
     * @param inserir
     * @throws SQLException
     */
    @Override
    public void insereix(Estudi inserir) throws SQLException {
        conexioJBDC.connection();
        String sentenciaSQL = "insert into ESTUDI (nom,cif,ciutat,data_alta) values (?,?,?,?)";
        PreparedStatement sentenciaPreparada = conexioJBDC.connection.prepareStatement(sentenciaSQL);
        sentenciaPreparada.setString(1, inserir.nom);
        sentenciaPreparada.setString(2, inserir.cif);
        sentenciaPreparada.setString(3, inserir.ciutat);
        sentenciaPreparada.setDate(4, (Date) inserir.data);

        sentenciaPreparada.executeUpdate();
        conexioJBDC.close();
    }

    /**
     * Mètode per recuperar per Id.
     *
     * @param id
     * @return
     * @throws SQLException
     */
    @Override
    public Estudi recuperaPerId(String id) throws SQLException {
        conexioJBDC.connection();
        Estudi estudi = new Estudi();

        String sentencia = "select * from ESTUDIS where CIF = "+id+";";
        Statement statementCl = this.conexioJBDC.connection.createStatement();
        ResultSet rsEst = statementCl.executeQuery(sentencia);
        estudi.nom = rsEst.getString("NOM");
        estudi.cif = rsEst.getString("CIF");
        estudi.ciutat = rsEst.getString("CIUTAT");
        estudi.data = rsEst.getDate("DATA_ALTA");

        conexioJBDC.close();
        return estudi;
    }

    /**
     * Mètode per recuperar dades.
     *
     * @return
     * @throws SQLException
     */
    @Override
    public List<Estudi> recuperarTots() throws SQLException {
        conexioJBDC.connection();
        ArrayList<Estudi> estudis = new ArrayList<>();

        String sentencia = "select * from ESTUDIS";
        Statement statementCl = this.conexioJBDC.connection.createStatement();
        ResultSet rsEst = statementCl.executeQuery(sentencia);

        while (rsEst.next()){
            Estudi estudi = new Estudi();
            estudi.nom = rsEst.getString("NOM");
            estudi.ciutat = rsEst.getString("CIUTAT");
            estudi.cif = rsEst.getString("CIF");
            estudi.data = rsEst.getDate("DATA_ALTA");
            estudis.add(estudi);
        }
        conexioJBDC.close();
        return estudis;

    /**
     * Mètode per eliminar.
     *
     * @param id
     * @throws SQLException
     */
    @Override
    public void eliminar(String id) throws SQLException {
        conexioJBDC.connection();
        String delete = "delete from ESTUDI WHERE id = " + id + ";";
        Statement statement = null;
        statement.execute(delete);
        try {
            statement = conexioJBDC.connection.createStatement();
        } catch (SQLException throwables) {
            excepcions.ConnexioException();
        }
    }

    /**
     * Mètode per actualitzar.
     *
     * @param estudi
     * @throws SQLException
     */
    @Override
    public void update(Estudi estudi) throws SQLException {
        String update = "update ESTUDIS set " + estudi.nom + "," + estudi.cif + "," + estudi.ciutat + "," + estudi.data + "where id = " + estudi.id + ";";
        Statement statement = null;
        statement.execute(update);
    }
}
