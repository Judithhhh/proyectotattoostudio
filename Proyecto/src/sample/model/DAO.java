import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

public interface DAO<T, I> {
    
    /**
     * @author Marti Serrallach
     */

    public void alta () throws SQLException, ParseException;
    public void insereix (T inserir) throws SQLException;
    public T recuperaPerId (I id);
    public List<T> recuperarTots ();
    public void eliminar (I id) throws SQLException;
    public void update (T estudi);

}
