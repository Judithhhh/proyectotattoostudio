import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Scanner;

public class DAOtatuadorJBDC implements DAOtatuador {
    /**
     * @author Marti Serrallach
     *
     * Mètodes que gestionen els tatuadors en JBDC.
     */
    ConexioJBDC conexioJBDC = new ConexioJBDC();
    Excepcions excepcions = new Excepcions();

    /**
     * Mètode per agregar Tatuador.
     *
     * @param inserir
     * @throws SQLException
     */
    @Override
    public void insereix(Tatuador inserir) throws SQLException {
        conexioJBDC.connection();
        String sentenciaSQL = "insert into TATUADOR (nom,dni,estil,estudi,actiu,instagram) values (?,?,?,?,?,?)";
        PreparedStatement sentenciaPreparada = conexioJBDC.connection.prepareStatement(sentenciaSQL);
        sentenciaPreparada.setString(1, inserir.nom);
        sentenciaPreparada.setString(2, inserir.nif);
        sentenciaPreparada.setString(3, inserir.estil);
        sentenciaPreparada.setString(4, inserir.estudi);
        sentenciaPreparada.setBoolean(5, inserir.actiu);
        sentenciaPreparada.setString(5, inserir.instagram);
        sentenciaPreparada.executeUpdate();
        conexioJBDC.close();
    }

    /**
     * Mètode per recuperar per Id.
     *
     * @param id
     * @return
     * @throws SQLException
     */
    @Override
    public Tatuador recuperaPerId(Integer id) throws SQLException {
        conexioJBDC.connection();
        Tatuador tatuador = new Tatuador();

        String sentencia = "select + from TATUADORS where ID = " + id + ";";
        Statement statementCl = this.conexioJBDC.connection.createStatement();
        ResultSet rsTat = statementCl.executeQuery(sentencia);

        tatuador.nom = rsTat.getString("NOM");
        tatuador.nif = rsTat.getString("NIF");
        tatuador.estil = rsTat.getString("ESTIL");
        tatuador.estudi = rsTat.getString("ESTUDI");
        tatuador.actiu = rsTat.getBoolean("ACTIU");
        tatuador.instagram = rsTat.getString("INSTAGRAM");

        conexioJBDC.close();
        return tatuador;
    }

    /**
     * Mètode per recuperar dades.
     *
     * @return
     * @throws SQLException
     */
    @Override
    public List<Tatuador> recuperarTots() throws SQLException {
        conexioJBDC.connection();
        ArrayList<Tatuador> llista = new ArrayList<>();

        String sentencia = "select * from TATUADORS;";
        Statement statementCl = this.conexioJBDC.connection.createStatement();
        ResultSet rsTat = statementCl.executeQuery(sentencia);

        while (rsTat.next()){
            Tatuador tatuador = new Tatuador();
            tatuador.nom = rsTat.getString("NOM");
            tatuador.nif = rsTat.getString("NIF");
            tatuador.estil = rsTat.getString("ESTIL");
            tatuador.estudi = rsTat.getString("ESTUDI");
            tatuador.actiu = rsTat.getBoolean("ACTIU");
            tatuador.instagram = rsTat.getString("INSTAGRAM");

            llista.add(tatuador);
        }
        conexioJBDC.close();
        return llista;
    }

    /**
     * Mètode per eliminar.
     *
     * @param id
     * @throws SQLException
     */
    @Override
    public void eliminar(Integer id) throws SQLException {
        conexioJBDC.connection();

        String delete = "delete from TATUADOR WHERE id = " + id + ";";
        Statement statement = null;
        statement.execute(delete);
        try {
            statement = conexioJBDC.connection.createStatement();
        } catch (SQLException throwables) {
            excepcions.ConnexioException();
        }
        conexioJBDC.close();
    }

    /**
     * Mètode per actualitzar.
     *
     * @param tatuador
     * @throws SQLException
     */
    @Override
    public void update(Tatuador tatuador) throws SQLException {
        String update = "update ESTUDIS set " + tatuador.nom + "," + tatuador.nif + "," + tatuador.estil + "," + tatuador.estudi + "," + tatuador.actiu + "," + tatuador.instagram + "where id = " + tatuador.id + ";";
        Statement statement = null;
        statement.execute(update);
    }
}
