/**
 * @author Judith Archilla
 */

public class Tatuador {

    /**
	 * Nom del tatuador.
	 */
    String nom;

    /**
	 * Nif del tatuador.
	 */
    int nif;

    /**
	 * Estil del tatuador.
	 */
    String estil;

    /**
	 * Estudi del tatuador.
	 */
    String estudi;

    /**
	 * Actiu del tatuador.
	 */
    boolean actiu;

    /**
	 * Id del tatuador.
	 */
    int id;

    /**
	 * Instagram del tatuador.
	 */
    String instagram;


    /**
     * Constructor
     */
    public Tatuador(String nom, String dni, String estil, String estudi, boolean actiu, String instagram) {
        this.nom = nom;
        this.nif = nif;
        this.estil = estil;
        this.estudi = estudi;
        this.actiu = actiu;
        this.instagram = instagram;
    }

    /**
     * Constructor vacio.
     */
    public Tatuador() {
    }
}
