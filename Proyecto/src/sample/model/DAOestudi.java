import java.util.List;

public interface DAOestudi extends DAO<Estudi, String> {
    /**
     * @author Marti Serrallach
     */
    public List<Estudi> cercaPerNom (String nom);
}
