import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import org.bson.Document;
import static com.mongodb.client.model.Filters.gt;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
    
    /**
     * @author Marti Serrallach & Judith Archilla
     * 
     * Mètodes que gestionen els estudis en Mongo.
     */

public class DAOestudiMONGO implements DAOestudi {
    
    ConexioMongo conexioMongo = new ConexioMongo();

    /**
     * Mètode per a buscar un estudi per nom.
     *
     * @param nom
     * @return llista de noms
     */
    @Override
    public List<Estudi> cercaPerNom(String nom) {
        List<Estudi> estudis = new ArrayList<>();
        MongoDatabase mongoDatabase = conexioMongo.connection();
        MongoCollection collection = mongoDatabase.getCollection("studio");
        try (MongoCursor<Document> cursor = collection.find(gt("nom", nom)).iterator()) {
            while (cursor.hasNext()) {
                Estudi estudi = new Estudi();
                Document document = cursor.next();
                estudi.id = document.getInteger("id");
                estudi.nom = document.getString("nom");
                estudi.ciutat = document.getString("ciutat");
                estudi.cif = document.getString("cif");
                estudi.data = document.getDate("data");
                estudis.add(estudi);
            }
        } finally {
            conexioMongo.close();
        }
        return estudis;
    }

    /**
     * Mètode per recuperar per Id.
     *
     * @param id identificador.
     * @return
     */
    @Override
    public Estudi recuperaPerId(String id) {
        Estudi estudi = new Estudi();
        MongoDatabase mongoDatabase = conexioMongo.connection();
        MongoCollection collection = mongoDatabase.getCollection("studio");
        try (MongoCursor<Document> cursor = collection.find(gt("id", id)).iterator()) {
            while (cursor.hasNext()) {
                Document document = cursor.next();
                estudi.id = document.getInteger("id");
                estudi.nom = document.getString("nom");
                estudi.ciutat = document.getString("ciutat");
                estudi.cif = document.getString("cif");
                estudi.data = document.getDate("data");
            }
        } finally {
            conexioMongo.close();
        }
        return estudi;
    }

    /**
     * Mètode per recuperar dades.
     *
     * @return llista de tots els estudis
     */
    @Override
    public List<Estudi> recuperarTots() {
        List<Estudi> estudis = new ArrayList<>();
        MongoDatabase mongoDatabase = conexioMongo.connection();
        MongoCollection collection = mongoDatabase.getCollection("studio");
        try (MongoCursor<Document> cursor = collection.find().iterator()) {
            while (cursor.hasNext()) {
                Estudi estudi = new Estudi();
                Document document = cursor.next();
                estudi.id = document.getInteger("id");
                estudi.nom = document.getString("nom");
                estudi.ciutat = document.getString("ciutat");
                estudi.cif = document.getString("cif");
                estudi.data = document.getDate("data");
                estudis.add(estudi);
            }
        } finally {
            conexioMongo.close();
        }
        return estudis;
    }

    /**
     * Mètode per a inserir dades.
     *
     * @param inserir
     */
    @Override
    public void insereix(Estudi inserir) {
        MongoDatabase mongoDatabase = conexioMongo.connection();
        MongoCollection<Document> collection = mongoDatabase.getCollection("studio");
        Document doc = new Document("id", inserir.id).append("nom", inserir.nom).append("ciutat", inserir.ciutat).append("cif", inserir.cif).append("data", inserir.data);
        collection.insertOne(doc);
        conexioMongo.close();
    }


    /**
     * Mètode per eliminar l'estudi per id.
     *
     * @param id identificador.
     */
    @Override
    public void eliminar(String id) {
        MongoDatabase mongoDatabase = conexioMongo.connection();
        MongoCollection<Document> collection = mongoDatabase.getCollection("studio");
        DeleteResult deleteResult = collection.deleteMany(gt("id", id));
        conexioMongo.close();
    }

    /**
     * Mètode per actualitzar.
     *
     * @param estudi
     */
    @Override
    public void update(Estudi estudi) {

    }
}