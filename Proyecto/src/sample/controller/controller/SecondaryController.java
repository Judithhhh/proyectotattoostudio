package controller.controller;
import project.App;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;

    /**
     * @author Judith Archilla
     */

public class SecondaryController {

    /**
     * En pressionar apareix la pantalla "tercera",  obrint-se en una nova pestanya.
     */
    @FXML
    private void showTercera() throws IOException {
        FXMLLoader loader = new FXMLLoader(App.class.getResource("forms/tercera.fxml"));
        Scene scene;
        try {
            scene = new Scene(loader.load());
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * En pressionar es dirigeix a la pantalla "primary", obrint-se en la mateixa pestanya.
     */
    @FXML
    private void switchToPrimary() throws IOException {
        App.setRoot("primary");
    }

    /**
     * En pressionar es dirigeix a la pantalla "escoger", obrint-se en la mateixa pestanya.
     */
    @FXML
    private void switchToEscoger() throws IOException {
        App.setRoot("escoger");
    }



}
