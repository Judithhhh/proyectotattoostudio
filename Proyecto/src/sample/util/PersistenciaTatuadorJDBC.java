import java.net.ConnectException;
import java.sql.SQLException;

    /**
     * @author Judith Archilla
     * 
     * (D'una classe hereta d'unes altres).
     */

public class PersistenciaTatuadorJDBC implements {

    @Override
    public void cercaPerNom(String nom) throws SQLException, ConnectException {

    }

    @Override
    public void insereix(DAOtatuadorJBDC daoTatuadorJBDC) throws SQLException, ConnectException {

    }

    @Override
    public void recuperarPerId(int id) throws SQLException, ConnectException {

    }

    @Override
    public void recuperarTots() throws SQLException, ConnectException {

    }

    @Override
    public void eliminar(DAOtatuadorJBDC daoTatuadorJBDC) throws SQLException, ConnectException {

    }

    @Override
    public void update(DAOtatuadorJBDC daoTatuadorJBDC) throws SQLException, ConnectException {

    }
    
}
