import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexioJBDC implements Conexio{

    /**
     * @author Marti Serrallach
     *
     * Funció que es connecta a la base de dades i es tanca (amb JBDC)
     */

    String url = "jdbc:mysql://localhost/TattooStudio";
    String user = "guest";
    String paswd = "guest";
    Connection connection = null;
    Excepcions excepcions = new Excepcions();

    @Override
    public void connection() {
        try {
            connection = DriverManager.getConnection(url,user,paswd);
        } catch (SQLException throwables) {
            excepcions.ConnexioException();
        }
    }

    @Override
    public void close() {
        try {
            connection.close();
        } catch (SQLException throwables) {
            excepcions.ConnexioException();
        }
    }

    @Override
    public void lleguirPropierties() {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(new File("/Users/PAPILLON/IdeaProjects/untitled/src/properties.properties")));
            this.url = (String) properties.get("URL");
            this.user = (String) properties.get("USER");
            this.passwd = (String) properties.get("PASSWD");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
