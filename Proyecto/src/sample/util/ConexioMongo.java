import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;

public class ConexioMongo implements Conexio{

    /**
     * @author Judith Archilla
     *
     * Funció que es connecta a la base de dades i es tanca (amb MongoBD).
     */

    MongoClient mongoClient = new MongoClient();

    /**
     * Perquè es connecti a la base de dades (tattoostudio).
     */
    @Override
    public MongoDatabase connection() {
        MongoDatabase database = mongoClient.getDatabase("tattoostudio");
        return database;
    }

    /**
     * Perquè es tanqui a la base de dades.
     */
    @Override
    public void close() {
        mongoClient.close();
    }

}
