package project.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import project.App;

import java.io.IOException;

public class TerceraController {

    /**
     * En pressionar es dirigeix a la pantalla "primary", obrint-se en la mateixa pestanya.
     */
    @FXML
    private void switchToPrimary() throws IOException {
        App.setRoot("primary");
    }

    /**
     * En pressionar es dirigeix a la pantalla "secondary", obrint-se en la mateixa pestanya.
     */
    @FXML
    private void switchToEditarE() throws IOException {
        App.setRoot("secondary");
    }

    /**
     * En pressionar es dirigeix a la pantalla "seis", obrint-se en la mateixa pestanya.
     */
    @FXML
    private void switchToEditarT() throws IOException {
        App.setRoot("seis");
    }

    /**
     * En pressionar es dirigeix a la pantalla "escoger", obrint-se en la mateixa pestanya.
     */
    @FXML
    private void switchToEscoger() throws IOException {
        App.setRoot("escoger");
    }
}
