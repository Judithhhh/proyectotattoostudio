package project.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import project.App;

import java.io.IOException;

public class PrimaryController {

    /**
     * En pressionar es dirigeix a la pantalla "secondary", obrint-se en la mateixa pestanya.
     */
    @FXML
    private void switchToSecondary() throws IOException {
        App.setRoot("secondary");
    }

    /**
     * En pressionar es dirigeix a la pantalla "escoger", obrint-se en la mateixa pestanya.
     */
    @FXML
    private void switchToEditar() throws IOException {
        App.setRoot("escoger");
    }

    /**
     * En pressionar es dirigeix a la pantalla "cinco", obrint-se en la mateixa pestanya.
     */
    @FXML
    private void switchToEliminar() throws IOException {
        App.setRoot("cinco");
    }

    /**
     * En pressionar apareix la pantalla "cuatro", obrint-se en una nova pestanya.
     */
    @FXML
    private void showCuatro() throws IOException {
        FXMLLoader loader = new FXMLLoader(App.class.getResource("forms/cuatro.fxml"));
        Scene scene;
        try {
            scene = new Scene(loader.load());
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


}
